# Bulk SMS Script for Jose Ortega PTA
# Create your message in the message.txt file

import phonenumbers
import csv, sys 
from twilio.rest import Client       

# Define message, recipients file lists, Twilio Credentials.
#MESSAGE_FILE = '/Users/David/Dropbox/carnival.txt'     # File containing text message
#MESSAGE_FILE = '/Users/David/Dropbox/sep_pta_ch.txt'     # File containing text message
#MESSAGE_FILE = '/Users/David/Dropbox/nov_pta.txt'     # File containing text message
#MESSAGE_FILE = '/Users/David/Dropbox/still_on.txt'     # File containing text message
#MESSAGE_FILE = '/Users/David/Dropbox/givingtues.txt'     # File containing text message
#MESSAGE_FILE = '/Users/David/Dropbox/joes_auction.txt'
#MESSAGE_FILE = '/Users/David/Dropbox/community.txt'
MESSAGE_FILE = '/Users/David/Dropbox/april_pta.txt'

#PHONES_FILE = '/Users/David/Dropbox/master_list.csv'    # File containing participant numbers
PHONES_FILE = '/Users/David/Dropbox/2018_PTA_PHONELIST.csv'
#PHONES_FILE = '/Users/David/Dropbox/testnumber'
#PHONES_FILE = '/Users/David/Dropbox/JOES_PTA_2018_CH.csv'
#phone_list = {'+14155955158', '+15103296673', '+14153594666', '+15103669874'}

SMS_LENGTH = 144 # Max length of one SMS message
MSG_COST = 0.0075   # Cost per message
account_sid = "get from twilion"
auth_token = "get from twilio"
from_num = "+1415*******" # 'From' number in Twilio

# opted-out phone numbers
drop_list = {+1415*******', '+1415*******', '+1415*******', '+1415*******', '+1415*******'}


def load_and_dedup(CSV_FILE): # Open the recipients CSV and get all the numbers out of it
    with open(CSV_FILE, 'r') as csvfile:
        peoplereader = csv.reader(csvfile)
        inputs = [p[0] for p in peoplereader]
        print('Inputs: ',str(len(inputs)))        
        numbers = set(inputs) # remove duplicate numbers
        print('Deduped: ',str(len(numbers)))

    # Reformat the phone numbers for Twilio in E164    
    newnumbers = []
    
    for num in numbers:
        newnum = phonenumbers.format_number(phonenumbers.parse(num,"US"),phonenumbers.PhoneNumberFormat.E164)
        newnumbers.append(newnum)

    # Dedup the phone number list
    deduped = set(newnumbers)
    return deduped

# Remove opted-out numbers
def remove_opt_out(list):
    phone_list = load_and_dedup(PHONES_FILE)
    print('Input phone list length: '+str(len(phone_list)))
    clean_phone_list = phone_list - drop_list
    print('Cleaned phone list length: '+str(len(clean_phone_list)))
    return(clean_phone_list)

def msg_eval(message_file,SMS_LENGTH): # Open the message file, evaluate cost, ask for approval
    # Now put your SMS in a file called message.txt, and it will be read from there.
    with open(MESSAGE_FILE, 'r') as content_file:
        sms = content_file.read()

    # Check we read a message OK
    if len(sms.strip()) == 0:
        print("SMS message not specified- please make a {}' file containing it. \r\nExiting!".format(MESSAGE_FILE))
        sys.exit(1)
    else:
        print("> SMS message to send: \n\n{}".format(sms))

    # How many segments is this message going to use?
    segments = int(len(sms.encode('utf-8')) / SMS_LENGTH) +1
    return sms, segments

def calcing(list,msg_cost,segments): # Calculate how much it's going to cost:
    messages = len(list)
    cost = msg_cost * segments * messages
    print("> {} messages of {} segments each will be sent, at a cost of ${} ".format(messages, segments, cost))

def check_n_send(list, sms): # Check you really want to send them
    confirm = input("Send these messages? [Y/n] ")
    if confirm[0].lower() == 'y':
        # Set up Twilio client
        client = Client(account_sid, auth_token)

        # Send the messages
        for num in list:
            # Send the sms text to the number from the CSV file:
            print("Sending to " + num)
            message = client.messages.create(to=num, from_=from_num, body=sms)
    print("Exiting!")

def sms_from_file(CSV_FILE,msg_cost, sms_length, message_file):
    list = load_and_dedup(CSV_FILE)
    clean_phone_list = remove_opt_out(list)
    sms, segments = msg_eval(message_file, sms_length)
    calcing(list,msg_cost, segments)
    check_n_send(list, sms)

def sms_from_list(list, msg_cost, sms_length, message_file):
    clean_phone_list = remove_opt_out(list)
    sms, segments = msg_eval(message_file,sms_length)
    calcing(list,msg_cost, segments)
    check_n_send(list, sms)

sms_from_file(PHONES_FILE,MSG_COST, SMS_LENGTH, MESSAGE_FILE)
#sms_from_list(phone_list,MSG_COST, SMS_LENGTH, MESSAGE_FILE)